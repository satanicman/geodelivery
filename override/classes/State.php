<?php

class State extends StateCore
{
    /** @var int Id geo delivery */
    public $id_geo_delivery;

    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        State::$definition['fields']['id_geo_delivery'] = array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId');
        parent::__construct($id, $id_lang, $id_shop);
    }
}
