    <select name="id_state" id="id_state">
        {foreach from=$states item=s}
            <option value="{$s.id_state}"{if isset($state) && $state->id == $s.id_state} selected="selected"{/if}>{$s.name}</option>
        {/foreach}
    </select>
    {include file='./productgeodelivery-ajax.tpl'}
