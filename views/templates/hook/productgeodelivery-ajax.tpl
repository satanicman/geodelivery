{if isset($delivery) && $delivery}
    <div id="geodelivery">
        {l s='Доставка в %s %s от %s' m='geodelivery' sprintf=[$state->name, $country->name, $delivery->cost]}
        {if $delivery->description}{l s='за %s' sprintf=$delivery->description m='geodelivery'}{/if}
    </div>
{else}
    {l s='Доставка в ваш регион недоступна' m='geodelivery'}
{/if}