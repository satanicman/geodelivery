{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/form/form.tpl"}

{block name="label"}
	{if $input.type == 'geo_deliveries'}

	{else}
		{$smarty.block.parent}
	{/if}
{/block}

{block name="legend"}
	<h3>
		{if isset($field.image)}<img src="{$field.image}" alt="{$field.title|escape:'html':'UTF-8'}" />{/if}
		{if isset($field.icon)}<i class="{$field.icon}"></i>{/if}
		{$field.title}
		<span class="panel-heading-action">
			{foreach from=$toolbar_btn item=btn key=k}
				{if $k != 'modules-list' && $k != 'back'}
					<a id="desc-{$table}-{if isset($btn.imgclass)}{$btn.imgclass}{else}{$k}{/if}" class="list-toolbar-btn" {if isset($btn.href)}href="{$btn.href}"{/if} {if isset($btn.target) && $btn.target}target="_blank"{/if}{if isset($btn.js) && $btn.js}onclick="{$btn.js}"{/if}>
						<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="{l s=$btn.desc}" data-html="true">
							<i class="process-icon-{if isset($btn.imgclass)}{$btn.imgclass}{else}{$k}{/if} {if isset($btn.class)}{$btn.class}{/if}" ></i>
						</span>
					</a>
				{/if}
			{/foreach}
			</span>
	</h3>
{/block}

{block name="input_row"}
	{if $input.type == 'select_states'}
        <div class="form-group">
            <div class="row">
                <div class="control-label col-lg-3">{$input.label}</div>
                <div class="col-lg-9">
                    <div class="panel">
                        <div class="tree-panel-heading-controls clearfix">
                            <div class="tree-actions pull-right">
                                <a href="#" onclick="$('#states ul').hide('fast');return false;" id="collapse-all-states" class="btn btn-default">
                                    <i class="icon-collapse-alt"></i>	{l s='Свернуть все' m='geodelivery'}
                                </a>
                                <a href="#" onclick="$('#states ul').show('fast');return false;" id="expand-all-states" class="btn btn-default">
                                    <i class="icon-expand-alt"></i>	{l s='Развернуть все' m='geodelivery'}
                                </a>
                            </div>
                        </div>
                        <ul id="states" class="cattree tree">
                            {foreach from=$input.options.states item=country}
                                {if $country.states}
                                    <li class="tree-folder">
                                        <span class="tree-folder-name" onclick="$(this).siblings('.tree').toggle('fast');return false;">
                                                    <i class="icon-folder-close"></i>
                                            <label class="tree-toggler">{$country.name}</label>
                                        </span>
                                        <ul class="tree" style="display: none;">
                                            {foreach from=$country.states item=state}
                                                <li class="tree-folder">
                                                    <span class="tree-folder-name">
                                                        <input type="checkbox" name="{$input.name}" value="{$state.id_state}"{if in_array($state.id_state, $input.options.selected)} checked="checked"{/if}>
                                                        <label class="tree-toggler">{$state.name}</label>
                                                    </span>
                                                </li>
                                            {/foreach}
                                        </ul>
                                    </li>
                                {/if}
                            {/foreach}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
	{elseif $input.type == 'geo_deliveries'}
		<div class="row">
			{assign var=deliveries value=$input.values}
			{if isset($deliveries) && count($deliveries) > 0}
				<table class="table">
                        <thead>
                            <tr class="nodrag nodrop">
                                <th>{l s='Cost' mod='geodelivery'}</th>
                                <th>{l s='Description' mod='geodelivery'}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach $deliveries as $key => $delivery}
                                <tr class="{if $key%2}alt_row{else}not_alt_row{/if} row_hover" id="tr_{$key%2}_{$delivery['id_geo_delivery']}">
                                    <td>{$delivery['cost']}</td>
                                    <td>{$delivery['description']}</td>
                                    <td>
                                        <div class="btn-group-action">
                                            <div class="btn-group pull-right">
                                                <a class="btn btn-default" href="{$current}&amp;token={$token}&amp;editGeoDelivery&amp;id_geo_delivery={(int)$delivery['id_geo_delivery']}" title="{l s='Edit' mod='geodelivery'}">
                                                    <i class="icon-edit"></i> {l s='Edit' mod='geodelivery'}
                                                </a>
                                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-caret-down"></i>&nbsp;
                                                </button>
                                                <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{$current}&amp;token={$token}&amp;deleteGeoDelivery&amp;id_geo_delivery={(int)$delivery['id_geo_delivery']}" title="{l s='Delete' mod='geodelivery'}">
                                                        <i class="icon-trash"></i> {l s='Delete' mod='geodelivery'}
                                                    </a>
                                                </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                    </table>
			{else}
				<p>{l s='No deliveries have been created.' mod='geodelivery'}</p>
			{/if}
		</div>
	{else}
		{$smarty.block.parent}
	{/if}
{/block}
