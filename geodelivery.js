$(document).ready(function () {
    $(document).on('change', '#id_state', function(e) {
        e.preventDefault();

        $.ajax({
            url: baseDir + 'modules/geodelivery/geodelivery-ajax.php',
            type: 'GET',
            dataType: 'json',
            data: {id_state: $(this).val()},
            success: function (data) {
                if(!data.hasError) {
                    $('#geodelivery').html(data.html);
                }
            }
        });
    });
});