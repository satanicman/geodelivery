<?php

class GeoDeliveryModel extends ObjectModel
{
    /**
     * @var float Delivery cost
     */
    public $cost;

    /**
     * @var string Delivery description
     */
    public $description;

    public static $definition = array(
        'table' => 'geo_delivery',
        'primary' => 'id_geo_delivery',
        'multilang' => true,
        'fields' => array(
            'cost'          =>  array('type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => true, 'size' => 128),
            'description'   =>  array('type' => self::TYPE_STRING, 'validate' => 'isString', 'lang' => true, 'size' => 128),
        ),
        'associations' => array(
            'state' =>                array('type' => self::HAS_ONE),
        ),
    );

    public function delete()
    {
        $this->clearStates();
        return parent::delete();
    }

    public static function createTables()
    {
        return (self::createTable() && self::createTableLang());
    }

    public static function addColumns()
    {
        return (self::addColumn('id_geo_delivery', 'state', 'INT UNSIGNED NULL'));
    }

    public static function dropColumns()
    {
        return (self::dropColumn('id_geo_delivery', 'state'));
    }

    public static function addColumn($column, $table, $params)
    {
        if(!self::columnExist($column, $table))
            return Db::getInstance()->query('ALTER TABLE `' . _DB_PREFIX_ . $table. '` ADD `' . $column . '` ' . $params);

    }

    public static function dropColumn($column, $table)
    {
        if(self::columnExist($column, $table))
            return Db::getInstance()->query('ALTER TABLE `' . _DB_PREFIX_ . $table . '` DROP `' . $column . '`');

    }

    public static function createTable()
    {
		$sql = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'geo_delivery`(
			`id_geo_delivery` int(10) unsigned NOT NULL auto_increment,
			`cost` varchar(128) NOT NULL,
			PRIMARY KEY (`id_geo_delivery`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

		return Db::getInstance()->execute($sql);
    }

    public static function createTableLang()
    {
		$sql = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'geo_delivery_lang`(
			`id_geo_delivery` int(10) unsigned NOT NULL,
			`id_lang` int(10) unsigned NOT NULL,
			`description` varchar(128) NULL,
			PRIMARY KEY (`id_geo_delivery`, `id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

		return Db::getInstance()->execute($sql);
    }

    public static function dropTables()
    {
        $sql = 'DROP TABLE
			`'._DB_PREFIX_.'geo_delivery`,
			`'._DB_PREFIX_.'geo_delivery_lang`';

		return Db::getInstance()->execute($sql);
    }

    private static function columnExist($field, $table)
    {
        return (bool)Db::getInstance()->getValue("
                SELECT COUNT(*) FROM information_schema.COLUMNS 
                WHERE TABLE_SCHEMA = '"._DB_NAME_."' 
                      AND TABLE_NAME = '"._DB_PREFIX_.$table."' 
                      AND COLUMN_NAME = '".$field."'");
    }

    public static function getAllDeliveries($id_lang)
    {
        $result = Db::getInstance()->executeS('SELECT * FROM `' . _DB_PREFIX_ . 'geo_delivery` gd
        LEFT JOIN `' ._DB_PREFIX_ .'geo_delivery_lang` gdl ON gdl.`id_geo_delivery` = gd.`id_geo_delivery`
        WHERE gdl.`id_lang` = ' . (int)$id_lang);

        return $result;
    }

    public static function getDelivery($id_geo_delivery)
    {
    }

    public static function getCountries()
    {
        return Country::getCountries(Context::getContext()->language->id, true);
    }

    public static function getDeliveryStates($id_geo_delivery)
    {
        $rows = Db::getInstance()->executeS('SELECT `id_state` FROM `' . _DB_PREFIX_ . 'state` WHERE `id_geo_delivery` = ' . (int)$id_geo_delivery);

        $result = array();
        foreach ($rows as $row) {
            $result[] = $row['id_state'];
        }

        return $result;
    }

    public function setStates($states)
    {
        if(!is_array($states))
            $states = array($states);

        $this->clearStates();

        foreach ($states as $id_state) {
            $r = new State((int)$id_state);

            if(Validate::isLoadedObject($r)) {
                $r->id_geo_delivery = $this->id;
                $r->update();
            }
        }
    }

    private function clearStates()
    {
        return Db::getInstance()->update('state', array('id_geo_delivery' => null), 'id_geo_delivery = ' . $this->id);
    }

}