<?php

if (!defined('_PS_VERSION_'))
    exit;

include_once(dirname(__FILE__) . '/GeoDeliveryModel.php');

class geoDelivery extends Module
{
	protected $_html;
	protected $_display;

    public function __construct()
    {
        $this->name = 'geodelivery';
        $this->tab = 'other';
        $this->version = '0.1';
        $this->author = 'Pinguin studio';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Delivery description');
        $this->description = $this->l('Add delivery data for state');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!Configuration::get('MUMODULE_NAME'))
            $this->warning = $this->l('No name provided');
    }

    public function install()
    {
        if (!$this->registerHook('displayProductButtons')
            || !GeoDeliveryModel::createTables()
            || !GeoDeliveryModel::addColumns()
            || !parent::install()
        )
            return false;

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall()
            || !GeoDeliveryModel::dropTables()
            || !GeoDeliveryModel::dropColumns()
        )
            return false;

        return true;

    }

	public function initToolbar()
	{
		$current_index = AdminController::$currentIndex;
		$token = Tools::getAdminTokenLite('AdminModules');
		$back = Tools::safeOutput(Tools::getValue('back', ''));
		if (!isset($back) || empty($back))
			$back = $current_index.'&amp;configure='.$this->name.'&token='.$token;

		switch ($this->_display)
		{
			case 'add':
				$this->toolbar_btn['cancel'] = array(
					'href' => $back,
					'desc' => $this->l('Cancel')
				);
				break;
			case 'edit':
				$this->toolbar_btn['cancel'] = array(
					'href' => $back,
					'desc' => $this->l('Cancel')
				);
				break;
			case 'index':
				$this->toolbar_btn['new'] = array(
					'href' => $current_index.'&amp;configure='.$this->name.'&amp;token='.$token.'&amp;addGeoDelivery',
					'desc' => $this->l('Add new')
				);
				break;
			default:
				break;
		}
		return $this->toolbar_btn;
	}
	protected function displayForm()
	{
		$this->_display = 'index';

		$this->fields_form[]['form'] = array(
			'tinymce' => false,
			'legend' => array(
				'title' => $this->l('Geo delivery configuration'),
				'icon' => 'icon-link'
			),
			'input' => array(
				array(
					'type' => 'geo_deliveries',
					'label' => $this->l('Geo deliveries list'),
					'name' => 'geo_deliveries',
					'values' => GeoDeliveryModel::getAllDeliveries($this->context->language->id),
				),
			),
		);

		$helper = $this->initForm();
		$helper->submit_action = '';
		$helper->title = $this->l('Geo delivery configuration');

		$this->_html .= $helper->generateForm($this->fields_form);

		return;
	}

	protected function displayAddForm()
	{
		$token = Tools::getAdminTokenLite('AdminModules');
		$back = Tools::safeOutput(Tools::getValue('back', ''));
		$current_index = AdminController::$currentIndex;
		$selected_states = array();
		if (!isset($back) || empty($back))
			$back = $current_index.'&amp;configure='.$this->name.'&token='.$token;

		if (Tools::isSubmit('editGeoDelivery') && Tools::getValue('id_geo_delivery'))
		{
			$this->_display = 'edit';
			$id_geo_delivery = (int)Tools::getValue('id_geo_delivery');
			$geoDelivery = new GeoDeliveryModel($id_geo_delivery);
			$selected_states = GeoDeliveryModel::getDeliveryStates($id_geo_delivery);
		}
		else
			$this->_display = 'add';

		$this->fields_form[0]['form'] = array(
			'tinymce' => false,
			'legend' => array(
				'title' => isset($geoDelivery) ? $this->l('Edit geo delivery.') : $this->l('New geo delivery'),
				'icon' => isset($geoDelivery) ? 'icon-edit' : 'icon-plus-square'
			),
			'input' => array(
				array(
					'type' => 'text',
					'label' => $this->l('Delivery cost'),
					'name' => 'cost',
				),
				array(
					'type' => 'text',
					'label' => $this->l('Delivery description'),
					'name' => 'description',
					'lang' => true,
				),
				array(
					'type' => 'select_states',
					'label' => $this->l('States'),
					'name' => 'id_states[]',
                    'options' => array(
                        'states' => GeoDeliveryModel::getCountries(),
                        'selected' => $selected_states,
                    )
				)
			),
			'buttons' => array(
				'cancelBlock' => array(
					'title' => $this->l('Cancel'),
					'href' => $back,
					'icon' => 'process-icon-cancel'
				)
			),
			'submit' => array(
				'name' => 'submit_' . $this->name,
				'title' => $this->l('Save'),
			)
		);

		$this->context->controller->getLanguages();
		foreach ($this->context->controller->_languages as $language)
		{
			if (Tools::getValue('description_'.$language['id_lang']))
				$this->fields_value['description'][$language['id_lang']] = Tools::getValue('description_'.$language['id_lang']);
			else if (isset($geoDelivery) && isset($geoDelivery->description[$language['id_lang']]))
				$this->fields_value['description'][$language['id_lang']] = $geoDelivery->description[$language['id_lang']];
			else
				$this->fields_value['description'][$language['id_lang']] = '';
		}

		if (Tools::getValue('cost'))
			$this->fields_value['cost'] = (int)Tools::getValue('cost');
		else if (isset($geoDelivery))
			$this->fields_value['cost'] = $geoDelivery->cost;
		else
            $this->fields_value['cost'] = '';

		$helper = $this->initForm();

		if (isset($id_geo_delivery))
		{
			$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name.'&id_geo_delivery='.$id_geo_delivery;
			$helper->submit_action = 'editGeoDelivery';
		}
		else
			$helper->submit_action = 'addGeoDelivery';

		$helper->fields_value = isset($this->fields_value) ? $this->fields_value : array();
		$this->_html .= $helper->generateForm($this->fields_form);

		return;
	}

	protected function initForm()
	{
		$helper = new HelperForm();

		$helper->module = $this;
		$helper->name_controller = 'geodelivery';
		$helper->identifier = $this->identifier;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->languages = $this->context->controller->_languages;
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
		$helper->default_form_language = $this->context->controller->default_form_language;
		$helper->allow_employee_form_lang = $this->context->controller->allow_employee_form_lang;
		$helper->toolbar_scroll = true;
		$helper->toolbar_btn = $this->initToolbar();

		return $helper;
	}

    public function getContent()
    {
		$this->_html = '';
		$this->_postProcess();

		if (Tools::isSubmit('addGeoDelivery') || Tools::isSubmit('editGeoDelivery'))
			$this->displayAddForm();
		else
			$this->displayForm();

        return $this->_html;
    }

	protected function _postValidation()
	{
		$this->_errors = array();

		if (Tools::isSubmit('submit_' . $this->name))
		{
			$this->context->controller->getLanguages();
			$states = Tools::getValue('id_states');

			if (!Tools::getValue('cost'))
				$this->_errors[] = $this->l('Invalid cost value.');
			if (!is_array($states))
				$this->_errors[] = $this->l('You must choose at least one state.');
			else
			{
				foreach ($states as $state)
					if (!Validate::isUnsignedId((int)$state))
						$this->_errors[] = $this->l('Invalid state ID.');
				foreach ($this->context->controller->_languages as $language)
					if (strlen(Tools::getValue('description_'.$language['id_lang'])) > 128)
						$this->_errors[] = $this->l('Description is too long.');
			}
		}
		else if (Tools::isSubmit('deleteGeoDelivery') && !Validate::isInt(Tools::getValue('id_geo_delivery')))
		{
			$this->_errors[] = $this->l('Invalid id_geo_delivery');
		}

		if (count($this->_errors))
		{
			foreach ($this->_errors as $err)
				$this->_html .= '<div class="alert alert-danger">'.$err.'</div>';

			return false;
		}
		return true;
	}

	protected function _postProcess()
	{
		if ($this->_postValidation() == false)
			return false;

		$this->_errors = array();
		if (Tools::isSubmit('submit_' . $this->name))
		{
			$this->context->controller->getLanguages();
			$id_geo_delivery = Tools::getValue('id_geo_delivery', null);
			$geoDelivery = new GeoDeliveryModel($id_geo_delivery);
			$geoDelivery->cost = Tools::getValue('cost');

            foreach ($this->context->controller->_languages as $language) {
                if ($description = Tools::getValue('description_'.$language['id_lang']))
                    $geoDelivery->description[$language['id_lang']] = $description;
            }

            if($geoDelivery->save()) {
                $states = Tools::getValue('id_states');
                if ($states) {
                    $geoDelivery->setStates($states);
                }
            }


			if (Tools::isSubmit('addGeoDelivery'))
				$redirect = 'addGeoDeliveryConfirmation';
			elseif (Tools::isSubmit('editGeoDelivery'))
				$redirect = 'editGeoDeliveryConfirmation';

			Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&'.$redirect);
		}
		elseif (Tools::isSubmit('deleteGeoDelivery') && Tools::getValue('id_geo_delivery'))
		{
			$id_geo_delivery = Tools::getvalue('id_geo_delivery');

			if ($id_geo_delivery)
			{
			    $geoDelivery = new GeoDeliveryModel($id_geo_delivery);
			    $geoDelivery->delete();

				Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&deleteGeoDeliveryConfirmation');
			}
			else
				$this->_html .= $this->displayError($this->l('Error: You are trying to delete a non-existing geo delivery.'));
		}
		elseif (Tools::isSubmit('addGeoDeliveryConfirmation'))
			$this->_html .= $this->displayConfirmation($this->l('Delivery data added.'));
		elseif (Tools::isSubmit('editGeoDeliveryConfirmation'))
			$this->_html .= $this->displayConfirmation($this->l('Delivery data edited.'));
		elseif (Tools::isSubmit('deleteGeoDeliveryConfirmation'))
			$this->_html .= $this->displayConfirmation($this->l('Deletion successful.'));

		if (count($this->_errors))
		{
			foreach ($this->_errors as $err)
				$this->_html .= '<div class="alert error">'.$err.'</div>';
		}
	}

	public function hookHeader($params)
    {
        $this->context->controller->addJS(($this->_path).'geodelivery.js');
    }

	public function hookDisplayProductButtons($params)
	{
		if (!$this->isCached('productgeodelivery.tpl', $this->getCacheId()))
		{
		    $cookie = $this->context->cookie;
		    $id_state = 0;
		    if(isset($cookie->id_state)) {
		        $id_state = $cookie->id_state;
            } elseif (@filemtime(_PS_GEOIP_DIR_._PS_GEOIP_CITY_FILE_)) {
                include(_PS_GEOIP_DIR_.'geoipcity.inc');

                $gi = geoip_open(realpath(_PS_GEOIP_DIR_._PS_GEOIP_CITY_FILE_), GEOIP_STANDARD);
//                $record = geoip_record_by_addr($gi, Tools::getRemoteAddr());
                $record = geoip_record_by_addr($gi, '85.90.223.31');

                $id_state = (int)State::getIdByIso($record->region);

                if($id_state && Validate::isUnsignedId($id_state)) {
                    $cookie->id_state = $id_state;
                    $cookie->write();
                }
            }

            $this->regVariables($id_state);
		}
		return $this->display(__FILE__, 'productgeodelivery.tpl', $this->getCacheId());
	}

	public function ajaxCall()
    {
        $id_state = (int)Tools::getValue('id_state');
        $this->regVariables($id_state);
        $result = $this->context->smarty->fetch(_PS_MODULE_DIR_ . $this->name .'/views/templates/hook/productgeodelivery-ajax.tpl');
        die(Tools::jsonEncode(array('hasError' => false, 'html' => $result)));
    }

    protected function regVariables($id_state)
    {
        if(isset($id_state) && Validate::isUnsignedId($id_state)) {
            $state = new State($id_state, $this->context->language->id);
            if (Validate::isLoadedObject($state) && Validate::isUnsignedId($state->id_geo_delivery)) {
                $this->context->smarty->assign(array(
                    'delivery' => new GeoDeliveryModel($state->id_geo_delivery, $this->context->language->id),
                    'country' => new Country($state->id_country, $this->context->language->id),
                    'state' => $state,
                    'states' => State::getStatesByIdCountry($state->id_country)
                ));
            }
        }
    }
}